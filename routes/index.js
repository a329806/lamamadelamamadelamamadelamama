const express = require('express');
const router = express.Router();
const controler = require('../controlers/index');

/* GET home page. */
router.get('/', controler.home);

module.exports = router;
